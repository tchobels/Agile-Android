package com.example.alexisbels.ilearn;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexisbels on 28/03/17.
 */

public class SetConnexionRest {

    boolean connexion=false;
    String message;

    public void appelRestUser( final String login, final String mdp, MainActivity main, Response.Listener<String> listener,Response.ErrorListener errorlistener){
        RequestQueue rq= Volley.newRequestQueue(main);
        String url="http://91.121.239.214/secure/who";
        StringRequest strRequest=new StringRequest(Request.Method.GET,url,listener,errorlistener){
            public Map<String,String> getHeaders() throws AuthFailureError{
                Map<String,String> params = new HashMap<>();
                params.put("Authorisation","Basic "+login+":"+mdp);
                return params;
            }
        };
    }
}
