package com.example.alexisbels.ilearn;

/**
 * Created by alexisbels on 28/03/17.
 */

public class Stats {
    String categ;
    String titre;
    int nbcompil;
    int progress;

   public String getCateg(){
       return categ;
   }
   public String getTitre(){
       return titre;
   }
   public int getNbcompil(){
    return nbcompil;
   }
   public int getProgress(){
        return progress;
   }

   public void setCateg(String categ){
       this.categ=categ;
   }
   public void setTitre(String titre){
       this.titre=titre;
   }
   public void setNbcompil(int nbcompil){
       this.nbcompil=nbcompil;
   }
   public void setProgress(int progress){
       this.progress=progress;
   }
}
